# Conclusion

- Provider service developers can't make any changes without notifying Consumer service developer
  
- Consumer service developers can't expect other than what is there in contract from Provider service
  
- If Consumer service developers expect different behaviour mentioned in the existing contract from the Provider Service then they have to notify their expectation to Provider service developers, which in turn Provider service developer do the necessary api changes and update the contract verification tests
  
- Prevent breaking of service because all developers of Provider and Consumer service are in sync

- Contract testing is more reliable because it enables communication between both parties.