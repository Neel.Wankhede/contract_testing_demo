
## View this documentation in Obsidian

1. Install obsidian `brew install --cask obsidian`
2. Clone this repo `git clone https://gitlab.com/Neel.Wankhede/contract_testing_demo.git`
3. Open this repo as vault in obsidian `File` --> `Open Vault` --> `Open Folder as Vault` --> Select the cloned repo


# Summary
 
---

![[1. What is Contract Testing ?]] 

---

![[2. Where contract testing fit in the pyramid ?]]

---

![[3. Types of Contracts]]

---

![[4. Pact]]

---

![[5. Markers]]

---

![[6. Contract Testing Workflow]]

---

![[7. Contract testing workflow canvas.canvas|5. Contract testing workflow canvas]]

---

![[8. What if Consumer changes the contract without notifying provider ?.canvas|6. What if Consumer changes the contract without notifying provider ?]]

---

![[9. What if provider service developer do changes in code and did not convey to consumer developers ?.canvas|7. What if provider service developer do changes in code and did not convey to consumer developers.]]

---

![[10. Conclusion]]

---

![[11. Advantage over e2e tests]]

---